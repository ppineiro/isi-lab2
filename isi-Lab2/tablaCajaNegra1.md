# Tabla caja negra 1

| Descripcion entradas|  Nombre del test |
| ---- | ---- |
| Elemento no tipo lista| A1|
| Lista con menos de 2 numeros enteros | A2|
| Dos numeros y 1 decimal| B |
| 2 numeros orden ascendente | C |
| 2 numeros orden descendente|  D |
| 3 numeros orden ascendente |  E |
| 3 numeros orden descendente| F |
| 3 numeros: 1º el mayor  y el 2º el menor | G |
| 3 números: el 1º el menor y el 2º el mayor| H |
| 3 números:. 2º el mayor y 3º el menor |I |
| 3 números: 2º el menor y 3º el mayor | J |
| 5 números: 3º,4º o 5º que no sea de tipo entero|K|
