# Tabla caja blanca

| Alternativa | Propiedades de las entradas | Nombre del test |
| ---- | ---- | ---- |
| # 1 True | Tipo de a diferente a lista | A1|
| # 1 True | Longitud de a menor a 1 | A2|
| # 1 False | Tipo de a diferente a lista Y longitud de a mayor o igual a 1 | B |
| # 2 True | longitud a es 1 | C |
| # 2 False | Longitud a no es 1 | D |
| # 3 True | Valor e es menor a min1 | E |
| # 3 False | Valor e igual a min1 | F1 |
| # 3 False | Valor e mayor a min1 | F2 |
| # 4 0 veces | len(a) < 2 | G1 |
| # 4 0 veces | len(a) = 2 | G2 |
| # 4 1 vez | len(a) = 3 | H |
| # 4 más de 1 vez | len(a) > 3 | I|
| # 5 True | e < min1 | J|
| # 5 False | e = min1 | K1 |
| # 5 False | e > min1 | K2 |
| # 6 True | e < min2 | L |
| # 6 False | e = min2 | M1|
| # 6 False | e > min2 | M2 |
