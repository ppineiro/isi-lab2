class InvalidArgument(Exception):
	"Function called with invalid arguments"
	pass

def max_dist (a = None):
	if type(a) != list or len(a) < 2:
		raise InvalidArgument()
		
	if type(a[0])!= int or type(a[1])!=int:
		raise TypeError
			
	num = a[0]
	num1 = a[1]
	max_dist = abs(num-num1)
	
	for i in range(2, len(a)):
		
		if type(a[i])!=int:
			raise TypeError
			
		else:
			num=a[i]
			dist_aux = abs(num-num1)
			if dist_aux > max_dist:
				max_dist = dist_aux
			
			num1 = num
			
	return max_dist
