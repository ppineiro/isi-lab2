from datetime import datetime

class InvalidArgument(Exception):
	"Function called with invalid arguments"
	pass
	
def esBis(y):
        return y % 4 == 0 and y % 100 != 0 or y % 400 == 0
	
def day_of_year(d = None, m = None, y = None):
	
	if d == None or m == None or y == None:
		raise InvalidArgument
	
	fecha = str(d) + "/" + str(m) + "/" + str(y)
	
	try:
		fecha_dt = datetime.strptime(fecha, "%d/%m/%Y")
	except:
		raise InvalidArgument
		
	if esBis(y) == False:
		diasMes = [0,31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
	else:
		diasMes = [0,31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

	dias = 0
	total = 0
	i = 0

	while i < m:
		dias = dias + diasMes[i]
		i = i + 1
		
	total = d+ dias - 1
	return total 
	
