from dos_menores import dos_menores

def test_A1():
	
	assert dos_menores("a")== None

def test_A2():
	assert dos_menores([]) == None

def test_B ():
	assert dos_menores([3,2]) == (2,3)

def test_C():
	assert dos_menores([2]) == 2

def test_D ():
	assert dos_menores([4,7]) == (4,7)

def test_E():
	assert dos_menores([3,1]) == (1,3)

def test_F1():
	assert dos_menores([1,1]) == (1,1)

def test_F2():
	assert dos_menores([2,9]) == (2,9)

def test_G1():
	assert dos_menores([3]) == (3)

def test_G2():
	assert dos_menores([1,4]) == (1,4)

def test_H():
	assert dos_menores([1,3,5]) == (1,3)

def test_I():
	assert dos_menores([1,4,2,6]) == (1,2)

def test_J():
	assert dos_menores([2,4,1,5]) == (1,2)

def test_K1():
	assert dos_menores([1,2,1,4]) == (1,1)

def test_K2():
	assert dos_menores([1,2,2,5,4]) == (1,2)

def test_L():
	assert dos_menores([2,5,4,7,8]) == (2,4)

def test_M1():
	assert dos_menores([1,3,3,5,6]) == (1,3)

def test_M2():
	assert dos_menores([1,4,8,7,9]) == (1,4)

	
	

