from max_dist import max_dist, InvalidArgument
import pytest


@pytest.mark.parametrize("test_input,expected", [([5,7], 2), ([10,5],5), ([9, 5, 4], 4),([2,5,8],3), ([10,-5,5], 15), 
	([2,10, 4], 8), ([12, 20, 1], 19), ([20, 4, 50], 46)])
	
def test_max_dist(test_input, expected):
    assert max_dist(test_input) == expected

def test_A1():
		with pytest.raises(InvalidArgument):
			assert max_dist("p"), "Function called with invalid arguments"

def test_A2():
		with pytest.raises(InvalidArgument):
			assert max_dist([2]), "Function called with invalid arguments"

def test_B(): 
		with pytest.raises(TypeError):
			assert max_dist([3, 10.3])

def test_k():
	with pytest.raises(TypeError):
			assert max_dist([2, 4, 5.5, 9, 6])
