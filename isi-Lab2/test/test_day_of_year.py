from day_of_year import day_of_year, InvalidArgument
import pytest


@pytest.mark.parametrize("day,month,year,expected", [(5, 5, 1999, 124), (15, 3, 2024, 74)])
	
def test_max_dist(day, month, year, expected):
    assert day_of_year(day,month,year) == expected

def test_A():
		with pytest.raises(InvalidArgument):
			assert day_of_year(), "Function called with invalid arguments"

def test_B():
		with pytest.raises(InvalidArgument):
			assert day_of_year(32, 3, 2019), "Function called with invalid arguments"

def test_C():
		with pytest.raises(InvalidArgument):
			assert day_of_year(31, 11, 1878), "Function called with invalid arguments"

def test_D():
		with pytest.raises(InvalidArgument):
			assert day_of_year(-11, 1, 2000), "Function called with invalid arguments"

def test_E():
		with pytest.raises(InvalidArgument):
			assert day_of_year(0, 7, 2014), "Function called with invalid arguments"
	
def test_F():
		with pytest.raises(InvalidArgument):
			assert day_of_year(8, 15, 2033), "Function called with invalid arguments"	
			
def test_G():
		with pytest.raises(InvalidArgument):
			assert day_of_year(8, -11, 2011), "Function called with invalid arguments"	
			
def test_H():
		with pytest.raises(InvalidArgument):
			assert day_of_year(8, 0, 2015), "Function called with invalid arguments"
			
def test_I():
		with pytest.raises(InvalidArgument):
			assert day_of_year(8, 1, -2094), "Function called with invalid arguments"
			
def test_J():
		with pytest.raises(InvalidArgument):
			assert day_of_year(8), "Function called with invalid arguments"	
			
def test_K():
		with pytest.raises(InvalidArgument):
			assert day_of_year(4, 8), "Function called with invalid arguments"		
			
def test_M():
		with pytest.raises(InvalidArgument):
			assert day_of_year(22.342, 6, 2020), "Function called with invalid arguments"		
			
